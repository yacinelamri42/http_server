#!/usr/bin/env sh

CC=gcc
CFLAGS="-Wall -g -fsanitize=address -std=c11"
LIBS="-lc"
APPNAME="http_server"
PROJECT_ROOT=$(dirname $0)
[ -d $PROJECT_ROOT/bin ] || mkdir $PROJECT_ROOT/bin
$CC $CFLAGS -o $PROJECT_ROOT/bin/$APPNAME $PROJECT_ROOT/src/*.c $LIBS
