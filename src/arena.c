#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arena.h"

Arena* arena_create(size_t capacity) {
	if (capacity == 0) {
		capacity = DEFAULT_CAPACITY;
	}
	Arena* arena = calloc(1, sizeof(*arena));
	if (!arena) {
		goto end;
	}
	arena->start = calloc(capacity, 1);
	if (!arena->start) {
		goto end1;
	}
	arena->end = arena->start+capacity;
	arena->current = arena->start;
	return arena;
	end1:
	free(arena);
	end:
	return 0;
}

void* arena_allocate(Arena* arena, size_t size) {
	if (!size || !arena) {
		return 0;
	}
	if ((arena->end - arena->current) < size) {
		Arena temp = {0};
		temp.start = realloc(arena->start, (arena->end-arena->start)*2);
		if (!temp.start) {
			return 0;
		}
		temp.current = temp.start+(arena->current-arena->start);
		temp.end = temp.start+(arena->end-arena->start)*2;
		*arena = temp;
	}
	void* d = arena->current;
	arena->current+=size;
	return d;
}

void* arena_reallocate(Arena* arena, void* variable, size_t old_size, size_t new_size) {
	if (!arena || !variable) {
		return 0;
	}
	if (new_size <= old_size) {
		return variable;
	}
	void* new_variable = arena_allocate(arena, new_size);
	if (!new_variable) {
		return 0;
	}
	return memcpy(new_variable, variable, old_size);
}

void arena_destroy(Arena** arena) {
	free((*arena)->start);
	free(*arena);
	*arena = 0;
}

