#ifndef ARENA__H
#define ARENA__H

#include <stddef.h>
#define DEFAULT_CAPACITY 1024

typedef struct {
	void* start;
	void* current;
	void* end;
} Arena;

Arena* arena_create(size_t capacity);
void* arena_allocate(Arena* arena, size_t size);
void* arena_reallocate(Arena* arena, void* variable, size_t old_size, size_t new_size);
void arena_destroy(Arena** arena);


#endif
