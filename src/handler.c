#include "handler.h"
#include "arena.h"
#include "http.h"
#include <stdio.h>
#include <string.h>

int method_compare(const char* str1, const char* str2, size_t num) {
	return strncmp(str1, str2, num);
}

int path_compare(const char* str1, const char* str2, size_t num) {
	if (!str1 || !str2 || !num) {
		return -2;
	}
	// printf("%d\n", num);
	for (int i=1; i<=num; i++) {
		if (i == num && (str1[i] == '/' || str2[i] == '/')) {
			break;
		}
		// printf("%c %c\n", str1[i], str2[i]);
		if (str1[i] < str2[i]) {
			// printf("\n\n");
			return -1;
		}else if (str1[i] > str2[i]) {
			// printf("\n\n");
			return 1;
		}
	}
	// printf("==\n");
	return 0;
}

RequestHandler* request_handler_create(Arena* arena, const char* http_method, const char* http_request_target, void* args, int (*request_handler)(int client_fd, HTTPRequest* request, void* args)) {
	if (!arena || !http_method || !http_request_target || !request_handler) {
		return 0;
	}
	RequestHandler* handler = arena_allocate(arena, sizeof(*handler));
	if (!handler) {
		return 0;
	}
	char* method = arena_allocate(arena, strlen(http_method)+1);
	if (!method) {
		return 0;
	}
	char* target = arena_allocate(arena, strlen(http_request_target)+1);
	if (!target) {
		return 0;
	}
	strcpy(method, http_method);
	strcpy(target, http_request_target);
	handler->disabled = false;
	handler->http_request_target = target;
	handler->http_method = method;
	handler->request_handler = request_handler;
	handler->arena = arena;
	handler->http_method_len = strlen(method);
	handler->http_request_target_len = strlen(http_request_target);
	handler->args = args;
	return handler;
}

void request_handler_destroy(RequestHandler** handler) {
	(*handler)->disabled = true;
}

RequestHandlers* request_handlers_create(Arena* arena, size_t capacity, void* args, int (*default_handler)(int client_fd, HTTPRequest* request, void* args)){
	if (!arena || !default_handler) {
		return 0;
	}
	if (!capacity) {
		capacity = DEFAULT_CAPACITY;
	}
	RequestHandlers* handlers = arena_allocate(arena, sizeof(*handlers));
	if (!handlers) {
		return 0;
	}
	handlers->arena = arena;
	handlers->len = 0;
	handlers->capacity = capacity;
	handlers->request_handlers = arena_allocate(arena, sizeof(RequestHandler*)*capacity);
	handlers->default_handler = default_handler;
	handlers->args = args;
	if (!handlers->request_handlers) {
		return 0;
	}
	return handlers;
}

int request_handlers_add(RequestHandlers* handlers, const char* http_method, const char* http_request_target, void* args, int (*request_handler)(int client_fd, HTTPRequest* request, void* args)) {
	if (!handlers || !http_request_target || !http_method || !request_handler) {
		return 0;
	}
	if (handlers->len == handlers->capacity) {
		void* ptr = arena_reallocate(handlers->arena, handlers->request_handlers, handlers->capacity, handlers->capacity*2);
		if (!ptr) {
			return 0;
		}
		handlers->request_handlers = ptr;
		handlers->capacity*=2;
	}
	handlers->request_handlers[handlers->len++] = request_handler_create(handlers->arena, http_method, http_request_target, args, request_handler);
	return 1;
}

int request_handlers_remove(RequestHandlers* handlers, int index) {
	if (index < 0 || index >= handlers->len) {
		return 0;
	}
	request_handler_destroy(handlers->request_handlers+index);
	return 1;
}

int request_handlers_handle(RequestHandlers* handlers, HTTPRequest* request, int client_fd) {
	for (int i=0; i<handlers->len; i++) {
		if (!handlers->request_handlers[i]->disabled && 
			!method_compare(handlers->request_handlers[i]->http_method, request->http_method, request->http_method_len) &&
			!path_compare(handlers->request_handlers[i]->http_request_target, request->http_request_target, handlers->request_handlers[i]->http_request_target_len)) {
			return handlers->request_handlers[i]->request_handler(client_fd, request, handlers->request_handlers[i]->args);
		}
	}
	return handlers->default_handler(client_fd, request, handlers->args);
}

void request_handlers_destroy(RequestHandlers** handlers) {
	for (int i=0; i<(*handlers)->len; i++) {
		(*handlers)->request_handlers[i]->disabled = true;
	}
}

