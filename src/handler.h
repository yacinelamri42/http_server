#ifndef HANDLER__H
#define HANDLER__H

#include "arena.h"
#include "http.h"
#include <stdbool.h>

typedef struct {
	Arena* arena;
	char* http_method;
	size_t http_method_len;
	char* http_request_target;
	size_t http_request_target_len;
	int (*request_handler)(int client_fd, HTTPRequest* request, void* args);
	void* args;
	bool disabled;
} RequestHandler;

typedef struct {
	Arena* arena;
	RequestHandler** request_handlers;
	size_t len;
	size_t capacity;
	int (*default_handler)(int client_fd, HTTPRequest* request, void* args);
	void* args;
} RequestHandlers;

RequestHandlers* request_handlers_create(Arena* arena, size_t capacity, void* args, int (*default_handler)(int client_fd, HTTPRequest* request, void* args));
int request_handlers_add(RequestHandlers* handlers, const char* http_method, const char* http_request_target, void* args, int (*request_handler)(int client_fd, HTTPRequest* request, void* args));
int request_handlers_handle(RequestHandlers* handlers, HTTPRequest* request, int client_fd);
int request_handlers_remove(RequestHandlers* handlers, int index);
void request_handlers_destroy(RequestHandlers** handlers);

#endif // !HANDLER
