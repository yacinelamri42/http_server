#include "http.h"
#include "arena.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>


const char* status_code_text[] = {
	[100] = "Continue",
	[101] = "Switching protocols",
	[200] = "Ok",
	[201] = "Created",
	[202] = "Accepted",
	[203] = "Non-Authoritative Information",
	[204] = "No Content",
	[205] = "Reset Content",
	[206] = "Partial Content",
	[300] = "Multiple Choices",
	[301] = "Moved Permanently",
	[302] = "Found",
	[303] = "See Other",
	[304] = "Not Modified",
	[305] = "Use Proxy",
	[306] = "Reserved",
	[307] = "Temporary Redirect",
	[308] = "Permanenent Redirect",
	[400] = "Bad Request",
	[401] = "Unauthorized",
	[402] = "Payment Required",
	[403] = "Forbidden",
	[404] = "Not Found",
	[405] = "Method Not Allowed",
	[406] = "Not Acceptable",
	[407] = "Proxy Authentication Required",
	[408] = "Request Timeout",
	[409] = "Conflict",
	[410] = "Gone",
	[411] = "Length Required",
	[412] = "Precondition Failed",
	[413] = "Content Too Large",
	[414] = "URI Too Long",
	[415] = "Unsupported Media Type",
	[416] = "Range Not Satisfiable",
	[417] = "Exception Failed",
	[418] = "I am a tea pot",
	[421] = "Misdirected Request",
	[422] = "Unprocessable Content",
	[423] = "Locked",
	[424] = "Failed Dependency",
	[425] = "Too early",
	[426] = "Upgrade Required",
	[428] = "Precondition Required",
	[429] = "Too Many Requests",
	[431] = "Request Header Fields Too Large",
	[451] = "Unavailable For Legal Reasons",
	[500] = "Internal Server Error",
	[501] = "Not Implemented",
	[502] = "Bad Gateway",
	[503] = "Service Unavailable",
	[504] = "Gateway Timeout",
	[505] = "HTTP Version Not Supported",
	[506] = "Variant Also Negotiates",
	[507] = "Insufficient Storage",
	[508] = "Loop Detected",
	[510] = "Not Extended",
	[511] = "Network Authentication Required",
};

int get_header_index(HTTPRequest* request, const char* header) {
	for (int i=0; i<request->num_headers; i++) {
		if (!strncmp(request->headers[i]->key, header, request->headers[i]->key_len)) {
			return i;
		}
	}
	return -1;
}

HTTPRequest* http_request_parse(Arena* arena, const char* request_string, size_t len) {
	printf("------DEBUG-----\n");
	printf("%s\n", request_string);
	printf("------DEBUG-----\n");
	HTTPRequest* request = arena_allocate(arena, sizeof(*request));
	if (!request) {
		return 0;
	}
	int current_str = 0;
	char* start = (char*)request_string;
	char* end = 0;
	int i=0;
	for (i=0; i<len && request_string[i] != '\n'; i++) {
		if (request_string[i] == ' ' || request_string[i] == '\r') {
			end = (char*)request_string+i;
			char* string = arena_allocate(arena, end-start+1);
			if (!string) {
				return 0;
			}
			memcpy(string, start, end-start);
			string[end-start] = 0;
		switch (current_str++) {
				case 0:
					request->http_method = string;
					request->http_method_len = end-start;
				break;
				case 1:
					request->http_request_target = string;
					request->http_request_target_len = end-start;
				break;
				case 2:
					request->http_version = string;
					request->http_version_len = end-start;
				break;
				default:
					assert(0 && "the request is not supposed to come here");
				break;
			}
			start = end+1;
		}
	}
	start++;
	int key_val = 0;
	HTTPHeader* header = 0;
	request->num_headers = 0;
	for (; i<len; i++) {
		if (request_string[i] == ':' && key_val == 0) {
			end = (char*)request_string+i;
			char* string = arena_allocate(arena, end-start+1);
			if (!string) {
				return 0;
			}
			memcpy(string, start, end-start);
			string[end-start] = 0;
			header = arena_allocate(arena, sizeof(*header));
			if (!header) {
				return 0;
			}
			string[end-start] = 0;
			header->key = string;
			header->key_len = end-start;
			start = end+2;
			key_val++;
		}else if (request_string[i] == '\r' && key_val == 1) {
			end = (char*)request_string+i;
			char* string = arena_allocate(arena, end-start+1);
			if (!string) {
				return 0;
			}
			memcpy(string, start, end-start);
			string[end-start] = 0;
			request->headers[request->num_headers++] = header;
			header->value = string;
			header->value_len = end-start;
			start = end+2;
			if (*start == '\r') {
				i+=4;
				break;
			}
			key_val = 0;
		}
	}
	int n = get_header_index(request, "Content-Length");
	if (n == -1) {
		return request;
	}
	long num = strtol(request->headers[n]->value, 0, 10);
	if (num == 0) {
		return request;
	}
	request->http_content = arena_allocate(arena, num+1);
	int j;
	for (j=0; i<len && j<num; i++, j++) {
		request->http_content[j] = request_string[i];
	}
	request->http_content[j] = 0;
	request->http_content_length = num;
	return request;
}

void print_request(HTTPRequest* request) {
	printf("-----------------------------------\n");
	printf("Method: %s\n", request->http_method);
	printf("URL: %s\n", request->http_request_target);
	printf("Version: %s\n", request->http_version);
	for (int i=0; i<request->num_headers; i++) {
		printf("%s: %s\n", request->headers[i]->key, request->headers[i]->value);
	}
	printf("-------Content--------\n%s\n", request->http_content);
	printf("-----------------------------------\n");
}

