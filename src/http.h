#ifndef HTTP__H
#define HTTP__H

#include "arena.h"


typedef struct {
	char* key;
	size_t key_len;
	char* value;
	size_t value_len;
} HTTPHeader;

typedef struct {
	char* http_method;
	size_t http_method_len;
	char* http_request_target;
	size_t http_request_target_len;
	char* http_version;
	size_t http_version_len;
	HTTPHeader* headers[1024];
	size_t num_headers;
	char* http_content;
	size_t http_content_length;
} HTTPRequest;

typedef struct {
	char* http_version;
	size_t http_version_len;
	int status_code;
	HTTPHeader* headers[1024];
	size_t num_headers;
	char* http_content;
	size_t http_content_length;
} HTTPResponse;

int get_header_index(HTTPRequest* request, const char* header);
HTTPRequest* http_request_parse(Arena* arena, const char* request_string, size_t len);
void print_request(HTTPRequest* request);
char* compose_response(Arena* arena, size_t* response_len, const char* version, size_t version_len, int status_code, HTTPHeader** headers, size_t num_headers, const char* http_content, size_t http_content_length);


#endif
