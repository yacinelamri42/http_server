#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <threads.h>
#include "arena.h"
#include "http.h"
#include "handler.h"
#include <asm-generic/socket.h>

#define NUM_THREADS 32

char* directory_global;
RequestHandlers* request_handlers_global;

int handle_connection(void* args) {
	int client_fd = *(int*)args;
	Arena* arena = arena_create(16*1024);
	if (!arena) {
		return 1;
	}
	char request_string[1024];
	size_t request_string_len = recv(client_fd, request_string, 1024, 0);
	if (request_string_len == -1) {
		return 1;
	}
	HTTPRequest* request = http_request_parse(arena, request_string, request_string_len);
	if (!request) {
		return 1;
	}
	print_request(request);
	int return_val = request_handlers_handle(request_handlers_global, request, client_fd);
	arena_destroy(&arena);
	return return_val;
}

int not_found(int client_fd, HTTPRequest* request, void* args) {
	printf("404\n");
	if (client_fd == -1 || !request) {
		return 1;
	}
	char http_response_not_found[] = "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\n\r\n";
	send(client_fd, http_response_not_found, sizeof(http_response_not_found), 0);
	return 0;
}

int default_handler(int client_fd, HTTPRequest* request, void* args) {
	if (client_fd == -1 || !request || !args) {
		return 1;
	}
	Arena* arena = arena_create(16*1024);
	char filename[1024] = {0};
	int i=0;
	for (i=0; i<request->http_request_target_len; i++) {
		filename[i] = request->http_request_target[i];
	}
	char* dir = (char*)args;
	char directory[8192] = {0};
	strncpy(directory, dir, 1024);
	filename[i] = 0;
	strncat(directory, filename, 4096);
	printf("directory: %s\n", directory);
	FILE* file = fopen(directory, "r");
	if (!file) {
		not_found(client_fd, request, 0);
		arena_destroy(&arena);
		return 0;
	}
	fseek(file, 0, SEEK_END);
	size_t file_size = ftell(file);
	char* r = arena_allocate(arena, file_size+128);
	fseek(file, 0, SEEK_SET);
	int r_len = snprintf(r, 1024,  "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nContent-Length: %ld\r\n\r\n", file_size);
	for (i=0; i<file_size; i++) {
		char c = getc(file);
		r[r_len+i] = c;
	}
	r[r_len+i] = 0;
	fclose(file);
	send(client_fd, r, strlen(r), 0);
	arena_destroy(&arena);
	return 0;
}


int handler_post_files(int client_fd, HTTPRequest* request, void* args) {
	char* dir = (char*)args;
	char filename[1024] = {0};
	for (int i=7; i<request->http_request_target_len; i++) {
		filename[i-7] = request->http_request_target[i];
	}
	char directory[8192] = {0};
	strncpy(directory, dir, 8192);
	strncat(directory, filename, 1024);
	printf("|%s|\n", directory);
	FILE* file = fopen(directory, "w");
	if (!file) {
		not_found(client_fd, request, 0);
		return 1;
	}
	fprintf(file, "%s", request->http_content);
	char response[8192] = {0};
	int len = snprintf(response, 8192, "%s 201 Created\r\nLocation: %s\r\nContent-Length: 0\r\n\r\n", request->http_version, directory);
	send(client_fd, response, len, 0);
	fclose(file);
	return 0;
}

int handler_slash(int client_fd, HTTPRequest* request, void* args) {
	printf("/\n");
	if (client_fd == -1 || !request) {
		return 1;
	}
	char http_response_ok[] = "HTTP/1.1 200 OK\r\n\r\n";
	send(client_fd, http_response_ok, sizeof(http_response_ok), 0);
	return 0;
}

int handler_user_agent(int client_fd, HTTPRequest* request, void* args) {
	printf("user-agent\n");
	if (client_fd == -1 || !request) {
		return 1;
	}
	int index = get_header_index(request, "User-Agent");
	char r[1024] = {0};
	snprintf(r, 1024,  "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: %ld\r\n\r\n%s\r\n", request->headers[index]->value_len, request->headers[index]->value);
	send(client_fd, r, strlen(r), 0);
	return 0;
}

int handler_echo(int client_fd, HTTPRequest* request, void* args) {
	printf("echo\n");
	if (client_fd == -1 || !request) {
		return 1;
	}
	printf("entered\n");
	char data[1024] = {0};
	int i=0;
	for (i=6; i<request->http_request_target_len && request->http_request_target[i]; i++) {
		data[i-6] = request->http_request_target[i];
	}
	data[i] = 0;
	char r[1024] = {0};
	int len = snprintf(r, 1024,  "HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: %d\r\n\r\n%s\r\n", i-6, data);
	send(client_fd, r, len, 0);
	return 0;
}

int hander_get_files(int client_fd, HTTPRequest* request, void* args) {
	printf("files\n");
	if (client_fd == -1 || !request || !args) {
		return 1;
	}
	Arena* arena = arena_create(16*1024);
	char filename[1024] = {0};
	int i=0;
	for (i=7; i<request->http_request_target_len; i++) {
		filename[i-7] = request->http_request_target[i];
	}
	char* dir = (char*)args;
	char directory[8192] = {0};
	strncpy(directory, dir, 1024);
	filename[i] = 0;
	strncat(directory, filename, 4096);
	printf("directory: %s\n", directory);
	FILE* file = fopen(directory, "r");
	if (!file) {
		not_found(client_fd, request, 0);
		arena_destroy(&arena);
		return 0;
	}
	fseek(file, 0, SEEK_END);
	size_t file_size = ftell(file);
	char* r = arena_allocate(arena, file_size+128);
	fseek(file, 0, SEEK_SET);
	int r_len = snprintf(r, 1024,  "HTTP/1.1 200 OK\r\nContent-Type: application/octet\r\nContent-Length: %ld\r\n\r\n", file_size);
	for (i=0; i<file_size; i++) {
		char c = getc(file);
		r[r_len+i] = c;
	}
	r[r_len+i] = 0;
	fclose(file);
	send(client_fd, r, strlen(r), 0);
	arena_destroy(&arena);
	return 0;
}

int main(int argc, char** argv) {
	char dir[1024] = {0};
	if (argc < 2) {
		dir[0] = '.';
		dir[1] = '/';
		dir[2] = 0;
	}else{
		for (int i=1; i<argc; i++) {
			if (!strcmp(argv[i], "--directory") && (i+1)<argc) {
				memcpy(dir, argv[i+1], strlen(argv[i+1]));
				dir[strlen(argv[i+1])]= '/';
				dir[strlen(argv[i+1])+1]= 0;
			}
		}
	}
	directory_global = dir;
	setbuf(stdout, NULL);
	int server_fd, client_addr_len;
	struct sockaddr_in client_addr;
	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd == -1) {
		printf("Socket creation failed: %s...\n", strerror(errno));
		return 1;
	}
	int reuse = 1;
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(reuse)) < 0) {
		printf("SO_REUSEPORT failed: %s \n", strerror(errno));
		return 1;
	}
	struct sockaddr_in serv_addr = { 
		.sin_family = AF_INET ,
		.sin_port = htons(4221),
		.sin_addr = { 
			htonl(INADDR_ANY) 
		},
	};
	if (bind(server_fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) != 0) {
		printf("Bind failed: %s \n", strerror(errno));
		return 1;
	}
	int connection_backlog = 5;
	if (listen(server_fd, connection_backlog) != 0) {
		printf("Listen failed: %s \n", strerror(errno));
		return 1;
	}
	client_addr_len = sizeof(client_addr);
	Arena* arena = arena_create(16*1024);
	if (!arena) {
		return 1;
	}
	request_handlers_global = request_handlers_create(arena, 0, directory_global, default_handler);
	int res1 = request_handlers_add(request_handlers_global, "GET", "/", 0, handler_slash);
	int res2 = request_handlers_add(request_handlers_global, "GET", "/echo", 0, handler_echo);
	int res3 = request_handlers_add(request_handlers_global, "GET", "/user-agent", 0, handler_user_agent);
	int res4 = request_handlers_add(request_handlers_global, "GET", "/files", directory_global, hander_get_files);
	int res5 = request_handlers_add(request_handlers_global, "POST", "/files", directory_global, handler_post_files);
	if (!res1 || !res2 || !res3 || !res4) {
		printf("weird....\n");
		return 1;
	}
	thrd_t threads[NUM_THREADS];
	int clients[NUM_THREADS];
	for(;1;) {
		printf("Waiting for a client to connect...\n");
		for (int i=0; i<NUM_THREADS; i++) {
			clients[i] = accept(server_fd, (struct sockaddr *) &client_addr, &client_addr_len);
			thrd_create((threads+i), handle_connection, clients+i);
		}
		for (int i=0; i<NUM_THREADS; i++) {
			thrd_join(threads[i], 0);
			close(clients[i]);
		}
	}
	close(server_fd);
	arena_destroy(&arena);
	return 0;
}
